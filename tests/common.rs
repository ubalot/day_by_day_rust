extern crate neverending_do;

use neverending_do::{
    activity::{
        category::Category,
        commit::Commit,
        id::Id,
        visibility::{Visibility, ASAP},
        Activity,
    },
    database::{sqlite::SQLite, with_test_instance},
};

pub fn with_resetted_test_db_instance<F>(f: F)
where
    F: FnOnce(&mut SQLite),
{
    with_test_instance(|db| {
        if let Err(e) = db.reset() {
            panic!("{e:?}")
        };
        f(db)
    })
}

pub fn mock_activity() -> Activity {
    Activity {
        id: Id::new(None),
        title: String::from("test"),
        category: Category::Daily,
        visibility: Visibility::from(ASAP),
        last_commit: Commit::from(0),
    }
}
