extern crate neverending_do;

use common::with_resetted_test_db_instance;
use neverending_do::activity::Activity;

mod common;

#[test]
fn add_activity_to_db() {
    with_resetted_test_db_instance(|db| {
        let mut activity: Activity = common::mock_activity();

        assert!(db.add(&mut activity).is_ok());
    });
}

#[test]
fn readd_activity_to_db() {
    with_resetted_test_db_instance(|db| {
        let mut activity: Activity = common::mock_activity();
        let mut copy_1: Activity = activity.clone();

        assert!(db.add(&mut activity).is_ok());
        assert!(db.add(&mut copy_1).is_err());
    });
}

#[test]
fn not_find_activity_in_db() {
    with_resetted_test_db_instance(|db| {
        let activity: Activity = common::mock_activity();

        match db.find(activity.title, activity.category, activity.visibility) {
            Ok(res) => assert!(res.is_none()),
            Err(e) => {
                eprintln!("{e}");
                assert!(false);
            }
        }
    });
}

#[test]
fn find_activity_in_db() {
    with_resetted_test_db_instance(|db| {
        let mut activity: Activity = common::mock_activity();

        match db.add(&mut activity) {
            Ok(_) => {
                match db.find(
                    activity.title.clone(),
                    activity.category,
                    activity.visibility.clone(),
                ) {
                    Ok(res) => match res {
                        Some(a) => assert_eq!(a, activity),
                        None => assert!(false),
                    },
                    Err(e) => {
                        eprintln!("{e}");
                        assert!(false)
                    }
                }
            }
            Err(e) => {
                eprintln!("{e}");
                assert!(false)
            }
        }
    });
}

#[test]
fn find_activity_by_id_in_db() {
    with_resetted_test_db_instance(|db| {
        let mut activity: Activity = common::mock_activity();

        match db.add(&mut activity) {
            Ok(_) => match db.find_by_id(activity.id.into()) {
                Ok(a) => assert_eq!(a, activity),
                Err(e) => {
                    eprintln!("{e}");
                    assert!(false)
                }
            },
            Err(e) => {
                eprintln!("{e}");
                assert!(false)
            }
        }
    });
}

#[test]
fn remove_non_existing_activity_from_db() {
    with_resetted_test_db_instance(|db| {
        match db.get_activities() {
            Ok(activities) => assert_eq!(0, activities.len()),
            Err(e) => {
                eprintln!("{e}");
                assert!(false)
            }
        }

        let activity: Activity = common::mock_activity();
        match db.remove(activity) {
            Ok(size) => assert_eq!(size, 0),
            Err(e) => {
                eprintln!("{e}");
                assert!(false)
            }
        }
    });
}

#[test]
fn remove_activity_from_db() {
    with_resetted_test_db_instance(|db| {
        let mut activity: Activity = common::mock_activity();

        match db.add(&mut activity) {
            Ok(_) => match db.remove(activity) {
                Ok(size) => assert_eq!(size, 1),
                Err(e) => {
                    eprintln!("{e}");
                    assert!(false)
                }
            },
            Err(e) => {
                eprintln!("{e}");
                assert!(false)
            }
        }
    });
}

#[test]
fn edit_activity() {
    with_resetted_test_db_instance(|db| {
        let mut activity: Activity = common::mock_activity();

        match db.add(&mut activity) {
            Ok(_) => assert!(true),
            Err(e) => {
                eprintln!("{e}");
                assert!(false)
            }
        }

        activity.title = activity.title + "2";
        match db.update(activity) {
            Ok(size) => assert_eq!(size, 1),
            Err(e) => {
                eprintln!("{e}");
                assert!(false)
            }
        }
    });
}

#[test]
fn edit_non_existing_activity() {
    with_resetted_test_db_instance(|db| {
        let activity: Activity = common::mock_activity();
        assert!(db.update(activity).is_err());
    });
}
