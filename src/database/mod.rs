use std::sync::Mutex;

use lazy_static::lazy_static;

use crate::database::sqlite::SQLite;

pub mod sqlite;

lazy_static! {
    static ref INSTANCE: Mutex<Option<SQLite>> = Mutex::new(None);
}

fn get_env_var(key: &str) -> String {
    match std::env::var(key) {
        Ok(value) => value,
        Err(e) => {
            eprintln!("{e:?}");
            std::process::exit(1)
        }
    }
}

pub fn with_instance_name<F, R>(db_name: &str, f: F) -> R
where
    F: FnOnce(&mut SQLite) -> R,
{
    let mut instance = INSTANCE.lock().unwrap();

    if instance.is_none() {
        let db = if db_name == "ND_DB" {
            SQLite::new(get_env_var(db_name))
        } else {
            SQLite::new("ND_DB_TEST".to_string())
        };
        *instance = Some(db);
    }

    f(instance.as_mut().unwrap())
}

pub fn with_instance<F, R>(f: F) -> R
where
    F: FnOnce(&mut SQLite) -> R,
{
    with_instance_name("ND_DB", f)
}

pub fn with_test_instance<F>(f: F)
where
    F: FnOnce(&mut SQLite),
{
    with_instance_name("ND_DB_TEST", f)
}
