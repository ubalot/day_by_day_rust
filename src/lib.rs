use serde_json::Value;
use std::fs::{remove_file, File, OpenOptions};
use std::io::prelude::*;
use std::path::{Path, PathBuf};

pub mod activity;
pub mod database;
pub mod ndconfig;

use activity::{category::Category, Activity};

pub fn print_activities(activities: Vec<Activity>) {
    let mut max_title_len: usize = 6; // length of header `title` + 1 for space
    for activity in activities.iter() {
        if activity.title.len() > max_title_len {
            max_title_len = activity.title.len();
        }
    }
    let category_len: usize = 12;
    let mut max_visibility_len: usize = 9; // length of header `next view`
    for activity in activities.iter() {
        if activity.human_readable_visibility().len() > max_visibility_len {
            max_visibility_len = activity.human_readable_visibility().len();
        }
    }
    let is_done_len: usize = 6;

    let header: String = format!(
        "{title:^max_title_len$} | {category:^category_len$} | {visibility:^max_visibility_len$} | {is_done:^is_done_len$}",
        title = "title",
        category = "category",
        visibility = "next view",
        is_done = "done"
    );

    let separator: String = format!(
        "{0:-<max_title_len$}-|-{0:-<category_len$}-|-{0:-<max_visibility_len$}-|-{0:-<is_done_len$}",
        "-"
    );

    let body: Vec<String> = activities
        .iter()
        .map(|activity| {
            let title: String = activity.title.clone();
            let category: String = activity.category.to_string();
            let visibility: String = match activity.category {
                Category::Daily => "tomorrow".to_string(),
                _ => activity.human_readable_visibility(),
            };
            let is_done: bool = activity.is_done_now();
            format!(
                "{} | {} | {} | {}",
                format_args!("{title:<max_title_len$}"),
                format_args!("{category:^category_len$}"),
                format_args!("{visibility:^max_visibility_len$}"),
                format_args!("{is_done:<is_done_len$}"),
            )
        })
        .collect::<Vec<String>>();

    println!(
        "{header}
{separator}
{}",
        body.join("\n")
    );
}

pub fn list_activities(
    category_filter: Option<Category>,
    not_done_filter: bool,
    visibility_filter: bool,
) -> Result<Vec<Activity>, String> {
    database::with_instance(|db| {
        let mut activities: Vec<Activity> = db
            .get_activities()
            .expect("Unable to retrieve activities from db.");

        // Apply filters to activities list

        if let Some(category) = category_filter {
            activities.retain(|activity| activity.category == category)
        }

        if not_done_filter {
            activities.retain(|activity| !activity.is_done_now());
        }

        if visibility_filter {
            activities.retain(|activity| activity.is_visible_now());
        }

        Ok(activities)
    })
}

pub fn todo_activities() -> Result<Vec<Activity>, String> {
    database::with_instance(|db| {
        let mut activities: Vec<Activity> = db
            .get_activities()
            .expect("Unable to retrieve activities from db.");

        activities.retain(|activity| {
            (!activity.is_done_now() && activity.is_visible_now())
                || activity.commit_is_too_old_now()
        });

        Ok(activities)
    })
}

fn write_to_file(path: PathBuf, contents: String) -> Result<(), String> {
    let path_as_str: String = path.as_path().display().to_string();
    if Path::new(&path).try_exists().unwrap_or_else(|error| {
        panic!(
            "Problem checking file existence: {} {:?}",
            path_as_str, error
        )
    }) {
        remove_file(&path)
            .unwrap_or_else(|error| panic!("Problem removing file: {} {:?}", path_as_str, error));
    }
    File::create(&path)
        .unwrap_or_else(|error| panic!("Problem creting file: {} {:?}", path_as_str, error));
    let mut file: File = OpenOptions::new()
        .write(true)
        .open(&path)
        .unwrap_or_else(|error| panic!("error while opening file: {} {:?}", path_as_str, error));
    file.write_all(contents.as_bytes())
        .unwrap_or_else(|error| panic!("error while writing file: {} {:?}", path_as_str, error));
    file.sync_data()
        .unwrap_or_else(|error| panic!("error while finilizing file: {} {:?}", path_as_str, error));
    Ok(())
}

pub fn export(path: PathBuf) -> Result<(), String> {
    database::with_instance(|db| {
        let activities: Vec<Activity> = db
            .get_activities()
            .expect("Unable to retrieve activities from db.");

        match serde_json::to_string_pretty(&activities) {
            Ok(contents) => write_to_file(path, contents),
            Err(e) => Err(e.to_string()),
        }
    })
}

fn read_from_file(path: PathBuf) -> Result<String, String> {
    let path_as_str: String = path.as_path().display().to_string();
    let mut file: File = File::open(&path)
        .unwrap_or_else(|error| panic!("Problem opening file: {} {:?}", path_as_str, error));
    let mut contents: String = String::new();
    file.read_to_string(&mut contents)
        .unwrap_or_else(|error| panic!("Problem reading file: {} {:?}", path_as_str, error));
    Ok(contents)
}

pub fn import(path: PathBuf) -> Result<(), String> {
    let contents: String = read_from_file(path)?;
    if contents.is_empty() {
        return Err("file is empty".to_string());
    }

    database::with_instance(|db| {
        if let Err(e) = db.reset() {
            return Err(e.to_string());
        }

        let data: Value = match serde_json::from_str(contents.as_str()) {
            Ok(data) => data,
            Err(e) => return Err(e.to_string()),
        };
        let objects = match data.as_array() {
            Some(array) => array,
            None => return Err("content is not an array".to_string()),
        };
        for object in objects.iter() {
            match object.as_object() {
                Some(obj) => {
                    db.add(&mut Activity::from(obj))
                        .expect("Unable to add actiity to db.");
                }
                None => return Err("not an object inside array".to_string()),
            }
        }
        Ok(())
    })
}

pub fn reset() -> Result<(), String> {
    database::with_instance(|db: &mut database::sqlite::SQLite| match db.reset() {
        Ok(()) => Ok(()),
        Err(e) => Err(e.to_string()),
    })
}
