use chrono::prelude::*;
use std::fmt;

use inquire::{
    min_length, set_global_render_config,
    ui::{Attributes, Color, RenderConfig, StyleSheet, Styled},
    DateSelect, InquireError, Select, Text,
};
use lazy_static::lazy_static;

use nd::{
    activity::{
        category::Category,
        commit::Commit,
        id::Id,
        visibility::{Visibility, ASAP},
        Activity,
    },
    database, list_activities, print_activities, todo_activities,
};
use neverending_do as nd;

#[derive(Debug, Copy, Clone)]
enum Action {
    Add,
    Cancel,
    Done,
    Edit,
    List,
    Remove,
    Todo,
}

impl fmt::Display for Action {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let action: &str = match self {
            Action::Add => "add",
            Action::Cancel => "quit",
            Action::Done => "commit",
            Action::Edit => "edit",
            Action::List => "list",
            Action::Remove => "remove",
            Action::Todo => "todo",
        };
        write!(f, "{}", action)
    }
}

lazy_static! {
    static ref ACTIONS: Vec<Action> = vec![
        Action::Todo,
        Action::Done,
        Action::List,
        Action::Add,
        Action::Edit,
        Action::Remove,
        Action::Cancel
    ];
    static ref CATEGORIES: Vec<Category> = vec![
        Category::Once,
        Category::Daily,
        Category::Weekly,
        Category::Monthly,
        Category::Quarterly,
        Category::Yearly
    ];
}

fn input_title() -> Option<String> {
    let user_input: Result<Option<String>, InquireError> = Text::new("title > ")
        .with_validator(min_length!(1, "Minimum of 1 characters"))
        .with_default("< No title >")
        .prompt_skippable();
    match user_input {
        Ok(title) => title,
        Err(e) => {
            eprintln!("{e}");
            None
        }
    }
}

fn select_activity(activities: Vec<Activity>) -> Option<Activity> {
    let selected_activity: Result<Activity, InquireError> =
        Select::new("Select activity:", activities).prompt();
    selected_activity.ok()
}

fn select_category() -> Option<Category> {
    let selected_category: Result<Category, InquireError> =
        Select::new("Select category:", CATEGORIES.to_vec()).prompt();
    selected_category.ok()
}

fn select_visibility(category: Category) -> Option<Visibility> {
    let now: DateTime<Utc> = Utc::now();
    let year: i32 = now.year();
    let mut month: u32 = now.month();
    let mut day: u32 = now.day();

    let start_year: i32;
    let start_month: u32;
    let mut start_day: u32;

    let end_year: i32;
    let end_month: u32;
    let end_day: u32;

    let help_msg: &str;

    match category {
        Category::Once => {
            start_year = year;
            start_month = month;
            start_day = day;

            end_year = NaiveDate::MAX.year();
            end_month = NaiveDate::MAX.month();
            end_day = NaiveDate::MAX.day();

            help_msg = "Choose visibility date";
        }
        Category::Daily => return Some(Visibility::from(ASAP)),
        Category::Weekly => {
            start_year = year;
            start_month = month;
            start_day = day;

            end_year = year;
            end_month = month;

            // find weekday offset
            let diff = now.weekday().number_from_monday() - 1;
            if start_day - diff > 0 {
                start_day -= diff;
            } else {
                start_day = 1;
            }

            end_day = start_day + 1 + (7 - diff) + 1/* is exluded from range */;

            help_msg = "Choose visibility day of the week";
        }
        Category::Monthly => {
            start_year = year;
            start_month = 1;
            start_day = 1;

            end_year = year;
            end_month = 1;
            end_day = 31;

            help_msg = "Choose visibility day inside the month";

            // for default
            month = 1;
            day = 1;
        }
        Category::Quarterly => {
            start_year = year;
            start_day = 1;
            end_year = year;
            if (1..=3).contains(&month) {
                start_month = 1;
                end_month = 3;
                end_day = 31;

                // for default
                month = 2;
                day = 1;
            } else if (4..=6).contains(&month) {
                start_month = 4;
                end_month = 6;
                end_day = 30;

                // for default
                month = 5;
                day = 1;
            } else if (7..=9).contains(&month) {
                start_month = 7;
                end_month = 9;
                end_day = 30;

                // for default
                month = 8;
                day = 1;
            } else {
                start_month = 9;
                end_month = 12;
                end_day = 31;

                // for default
                month = 11;
                day = 1;
            }

            help_msg = "Choose visibility through the range of 3 months";
        }
        Category::Yearly => {
            start_year = year;
            start_month = 1;
            start_day = 1;

            end_year = year;
            end_month = 12;
            end_day = 31;

            help_msg = "Choose visibility date";
        }
    }

    let date: Result<NaiveDate, InquireError> = DateSelect::new("Define visibility:")
        .with_default(NaiveDate::from_ymd_opt(year, month, day).unwrap())
        .with_min_date(NaiveDate::from_ymd_opt(start_year, start_month, start_day).unwrap())
        .with_max_date(NaiveDate::from_ymd_opt(end_year, end_month, end_day).unwrap())
        .with_week_start(Weekday::Mon)
        .with_help_message(help_msg)
        .prompt();

    match date {
        Ok(nd) => match category {
            Category::Once => {
                let dt: String = nd.to_string();
                let string: Vec<&str> = dt.split('-').collect::<Vec<&str>>();
                let visibility: String = format!(
                    "{}-{}-{}",
                    string.first().unwrap(),
                    string.get(1).unwrap(),
                    string.get(2).unwrap()
                );
                Some(Visibility::from(visibility))
                // year-month-day
            }
            Category::Daily => Some(Visibility::from(ASAP)),
            Category::Weekly => {
                let visibility = nd.weekday().to_string();
                Some(Visibility::from(visibility))
                // offset_day
            }
            Category::Monthly => {
                let visibility = format!("{}", nd.day());
                Some(Visibility::from(visibility))
                // day
            }
            Category::Quarterly => {
                let month_offset = nd.month0() % 3;
                let visibility = format!("{}-{}", month_offset, nd.day());
                Some(Visibility::from(visibility))
                // offset_month-day
            }
            Category::Yearly => {
                let dt = nd.to_string();
                let string = dt.split('-').collect::<Vec<&str>>();
                let visibility = format!("{}-{}", string.get(1).unwrap(), string.get(2).unwrap());
                Some(Visibility::from(visibility))
                // month-day
            }
        },
        Err(_) => None,
    }
}

fn interactive_add_activity() {
    let Some(title): Option<String> = input_title() else {
        return;
    };
    let Some(category): Option<Category> = select_category() else {
        return;
    };
    let Some(visibility): Option<Visibility> = select_visibility(category) else {
        return;
    };
    let mut activity: Activity = Activity {
        id: Id::new(None),
        title,
        category,
        visibility,
        last_commit: Commit::from(0),
    };
    database::with_instance(|db| match db.add(&mut activity) {
        Ok(_) => println!("Added activity '{activity}'"),
        Err(e) => eprintln!("Error while creating activity: {e}"),
    })
}

fn interactive_mark_done_activity() {
    let activities: Vec<Activity> = match todo_activities() {
        Ok(activities) => activities,
        Err(e) => {
            eprintln!("{e}");
            return;
        }
    };
    let Some(selected_activity): Option<Activity> = select_activity(activities) else {
        return;
    };
    database::with_instance(|db| {
        if let Err(e) = db.commit(selected_activity) {
            eprintln!("{e}");
        }
    })
}

fn interactive_edit_activity() {
    let activities: Vec<Activity> = match list_activities(None, false, false) {
        Ok(activities) => activities,
        Err(e) => {
            eprintln!("{e}");
            return;
        }
    };
    let Some(mut selected_activity): Option<Activity> = select_activity(activities) else {
        return;
    };

    let fields: Vec<&str> = vec!["title", "points", "category"];
    let selected_field: Result<&str, InquireError> = Select::new("Edit:", fields).prompt();
    let field: &str = match selected_field {
        Ok(field) => field,
        Err(e) => {
            eprintln!("{e}");
            return;
        }
    };

    match field {
        "title" => {
            selected_activity.title = match input_title() {
                Some(title) => title,
                None => return,
            }
        }
        "category" => {
            selected_activity.category = match select_category() {
                Some(category) => {
                    match select_visibility(category) {
                        Some(visibility) => selected_activity.visibility = visibility,
                        None => return,
                    }
                    category
                }
                None => return,
            }
        }
        _ => {
            eprintln!("no field selected");
            return;
        }
    }

    database::with_instance(|db| {
        if let Err(e) = db.update(selected_activity) {
            eprintln!("Error while updating activity: {e}")
        }
    })
}

fn interactive_remove_activity() {
    database::with_instance(|db| {
        let activities: Vec<Activity> = db
            .get_activities()
            .expect("Unable to retrieve activities from db.");
        let Some(activity) = select_activity(activities) else {
            return;
        };
        match db.remove(activity.clone()) {
            Ok(_) => println!("Removed activity {activity}"),
            Err(e) => eprintln!("Error while removing activity: {e}"),
        }
    })
}

fn get_render_config() -> RenderConfig<'static> {
    let mut render_config: RenderConfig = RenderConfig::default();
    render_config.prompt_prefix = Styled::new(">").with_fg(Color::LightRed);
    render_config.highlighted_option_prefix = Styled::new("➙").with_fg(Color::LightYellow);
    render_config.selected_checkbox = Styled::new("☑").with_fg(Color::LightGreen);
    render_config.scroll_up_prefix = Styled::new("⇞");
    render_config.scroll_down_prefix = Styled::new("⇟");
    render_config.unselected_checkbox = Styled::new("☐");

    render_config.error_message = render_config
        .error_message
        .with_prefix(Styled::new("❌").with_fg(Color::LightRed));

    render_config.answer = StyleSheet::new()
        .with_attr(Attributes::ITALIC)
        .with_fg(Color::LightYellow);

    render_config.help_message = StyleSheet::new().with_fg(Color::DarkYellow);

    render_config
}

pub fn run() -> Result<(), String> {
    set_global_render_config(get_render_config());
    loop {
        let selected_action: Result<Action, InquireError> =
            Select::new("", ACTIONS.to_vec()).prompt();
        let Ok(action): Result<Action, InquireError> = selected_action else {
            continue;
        };
        match action {
            Action::Add => interactive_add_activity(),
            Action::Done => interactive_mark_done_activity(),
            Action::Edit => interactive_edit_activity(),
            Action::List => match list_activities(None, false, false) {
                Ok(activities) if activities.is_empty() => println!("Activities list is empty..."),
                Ok(activities) => print_activities(activities),
                Err(e) => eprintln!("{e}"),
            },
            // Action::Todo => match list_activities(None, true, true) {
            //     Ok(activities) if activities.is_empty() => println!("Every activity is completed!"),
            //     Ok(activities) => print_activities(activities),
            //     Err(e) => eprintln!("{e}"),
            // },
            Action::Todo => match todo_activities() {
                Ok(activities) if activities.is_empty() => println!("Every activity is completed!"),
                Ok(activities) => print_activities(activities),
                Err(e) => eprintln!("{e}"),
            },
            Action::Remove => interactive_remove_activity(),
            Action::Cancel => break,
        }
    }
    Ok(())
}
