use std::fmt;

use rusqlite::types::{FromSql, FromSqlResult, ToSql, ToSqlOutput, ValueRef};
use rusqlite::Result;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialOrd, Serialize, Deserialize)]
pub struct Visibility(String);

pub const ASAP: &str = "ASAP";

impl fmt::Display for Visibility {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl From<serde_json::Value> for Visibility {
    fn from(value: serde_json::Value) -> Visibility {
        match value {
            serde_json::Value::String(visibility) => Visibility::from(visibility),
            serde_json::Value::Null => panic!("Can't convert null to visibility"),
            serde_json::Value::Bool(_) => panic!("Can't convert boolean to visibility"),
            serde_json::Value::Number(_) => panic!("Can't convert number to visibility"),
            serde_json::Value::Array(_) => panic!("Can't convert array to visibility"),
            serde_json::Value::Object(_) => panic!("Can't convert object to visibility"),
        }
    }
}

impl From<String> for Visibility {
    fn from(visibility: String) -> Visibility {
        Visibility(visibility)
    }
}

impl From<&str> for Visibility {
    fn from(visibility: &str) -> Visibility {
        Visibility(String::from(visibility))
    }
}

impl PartialEq for Visibility {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}
impl Eq for Visibility {}

impl FromSql for Visibility {
    fn column_result(value: ValueRef) -> FromSqlResult<Self> {
        String::column_result(value).map(Visibility::from)
    }
}

impl ToSql for Visibility {
    fn to_sql(&self) -> Result<ToSqlOutput> {
        Ok(self.0.clone().into())
    }
}
