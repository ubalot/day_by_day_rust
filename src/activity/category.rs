use std::fmt;

use rusqlite::types::{FromSql, FromSqlResult, ToSql, ToSqlOutput, ValueRef};
use rusqlite::Result;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum Category {
    Once,
    Daily,
    Weekly,
    Monthly,
    Quarterly,
    Yearly,
}

impl fmt::Display for Category {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let cateory: &str = match self {
            Category::Once => "once",
            Category::Daily => "daily",
            Category::Weekly => "weekly",
            Category::Monthly => "monthly",
            Category::Quarterly => "quarterly",
            Category::Yearly => "yearly",
        };
        write!(f, "{cateory}")
    }
}

impl From<serde_json::Value> for Category {
    fn from(value: serde_json::Value) -> Category {
        match value {
            serde_json::Value::String(category) => Category::from(category),
            serde_json::Value::Null => panic!("Can't convert null to category"),
            serde_json::Value::Bool(_) => panic!("Can't convert boolean to category"),
            serde_json::Value::Number(_) => panic!("Can't convert number to category"),
            serde_json::Value::Array(_) => panic!("Can't convert array to category"),
            serde_json::Value::Object(_) => panic!("Can't convert object to category"),
        }
    }
}

impl From<String> for Category {
    fn from(category: String) -> Category {
        Category::from(category.as_ref())
    }
}

impl From<&str> for Category {
    fn from(category: &str) -> Category {
        match category {
            "daily" => Category::Daily,
            "weekly" => Category::Weekly,
            "monthly" => Category::Monthly,
            "quarterly" => Category::Quarterly,
            "yearly" => Category::Yearly,
            "once" => Category::Once,
            _ => panic!("Category can't be created from: {}", category),
        }
    }
}

impl PartialEq for Category {
    fn eq(&self, other: &Self) -> bool {
        (*self as u16) == (*other as u16)
    }
}
impl Eq for Category {}

impl FromSql for Category {
    fn column_result(value: ValueRef) -> FromSqlResult<Self> {
        String::column_result(value).map(Category::from)
    }
}

impl ToSql for Category {
    fn to_sql(&self) -> Result<ToSqlOutput> {
        Ok(self.to_string().into())
    }
}
