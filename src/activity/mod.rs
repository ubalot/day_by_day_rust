use chrono::offset::LocalResult;
use chrono::prelude::*;
use chrono::Duration;
use chrono::MappedLocalTime;
use num_traits::FromPrimitive;
use std::fmt;
use time::util::is_leap_year;

use serde::{Deserialize, Serialize};

pub mod category;
pub mod commit;
pub mod id;
pub mod visibility;

use category::Category;
use commit::Commit;
use id::Id;
use visibility::Visibility;

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Activity {
    pub id: Id,
    pub title: String,
    pub category: Category,
    pub visibility: Visibility,
    pub last_commit: Commit,
}

impl fmt::Display for Activity {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.category {
            Category::Daily => write!(f, "{}, {}", self.title, self.category),
            _ => write!(
                f,
                "{}, {}, {}",
                self.title,
                self.category,
                self.human_readable_visibility()
            ),
        }
    }
}

impl From<&serde_json::Map<String, serde_json::Value>> for Activity {
    fn from(object: &serde_json::Map<String, serde_json::Value>) -> Self {
        let title: String = match object["title"].clone() {
            serde_json::Value::String(title) => title,
            serde_json::Value::Null => panic!("Can't convert null to title"),
            serde_json::Value::Bool(_) => panic!("Can't convert boolean to title"),
            serde_json::Value::Number(_) => panic!("Can't convert number to title"),
            serde_json::Value::Array(_) => panic!("Can't convert array to title"),
            serde_json::Value::Object(_) => panic!("Can't convert object to title"),
        };
        Activity {
            id: Id::new(None),
            title,
            category: Category::from(object["category"].clone()),
            visibility: Visibility::from(object["visibility"].clone()),
            last_commit: Commit::from(object["last_commit"].clone()),
        }
    }
}

impl Activity {
    pub fn human_readable_visibility(&self) -> String {
        match self.category {
            Category::Weekly => {
                let visibility: String = self.visibility.to_string();
                let weekday = match visibility.parse::<Weekday>() {
                    Ok(day) => match day {
                        Weekday::Mon => "monday",
                        Weekday::Tue => "tuesday",
                        Weekday::Wed => "wednesday",
                        Weekday::Thu => "thursday",
                        Weekday::Fri => "friday",
                        Weekday::Sat => "saturday",
                        Weekday::Sun => "sunday",
                    },
                    Err(_) => panic!("wrong visibility value for weekly"),
                };
                weekday.to_string()
            }
            Category::Monthly => {
                let visibility: String = self.visibility.to_string();
                let day: u32 = visibility.parse::<u32>().unwrap();
                let day_suffix: &str = match day {
                    0 => "st",
                    1 => "nd",
                    2 => "rd",
                    _ => "th",
                };
                format!("{}{} day", day, day_suffix)
            }
            Category::Quarterly => {
                let visibility: String = self.visibility.to_string();
                let parts: Vec<&str> = visibility.split('-').collect::<Vec<_>>();
                if parts.len() != 2 {
                    panic!("wrong visibility format for quarterly")
                }
                let offset_month: u32 = parts.first().unwrap().parse::<u32>().unwrap();
                let month_suffix: &str = match offset_month {
                    0 => "st",
                    1 => "nd",
                    2 => "rd",
                    _ => panic!("month offset out of range (0,2)"),
                };
                let day: u32 = parts.get(1).unwrap().parse::<u32>().unwrap();
                let day_suffix: &str = match day {
                    1 => "st",
                    2 => "nd",
                    3 => "rd",
                    _ => "th",
                };
                format!(
                    "{}{} month - {}{} day",
                    offset_month + 1,
                    month_suffix,
                    day,
                    day_suffix
                )
            }
            Category::Yearly => {
                let visibility: String = self.visibility.to_string();
                let parts: Vec<&str> = visibility.split('-').collect::<Vec<_>>();
                if parts.len() != 2 {
                    panic!("wrong visibility format for yearly")
                }
                let month: &str =
                    match Month::from_u32(parts.first().unwrap().parse::<u32>().unwrap()) {
                        Some(month) => match month {
                            Month::January => "January",
                            Month::February => "February",
                            Month::March => "March",
                            Month::April => "April",
                            Month::May => "May",
                            Month::June => "June",
                            Month::July => "July",
                            Month::August => "August",
                            Month::September => "September",
                            Month::October => "October",
                            Month::November => "November",
                            Month::December => "December",
                        },
                        None => panic!("wrong value for month"),
                    };
                let day: u32 = parts.get(1).unwrap().parse::<u32>().unwrap();
                let day_suffix: &str = match day {
                    1 => "st",
                    2 => "nd",
                    3 => "rd",
                    _ => "th",
                };
                format!("{} {}{}", month, day, day_suffix)
            }
            Category::Once => {
                let visibility: String = self.visibility.to_string();
                let parts: Vec<&str> = visibility.split('-').collect::<Vec<_>>();
                if parts.len() != 3 {
                    panic!("wrong visibility format for once")
                }
                let year: i32 = parts.first().unwrap().parse::<i32>().unwrap();
                let month: &str =
                    match Month::from_u32(parts.get(1).unwrap().parse::<u32>().unwrap()) {
                        Some(month) => match month {
                            Month::January => "January",
                            Month::February => "February",
                            Month::March => "March",
                            Month::April => "April",
                            Month::May => "May",
                            Month::June => "June",
                            Month::July => "July",
                            Month::August => "August",
                            Month::September => "September",
                            Month::October => "October",
                            Month::November => "November",
                            Month::December => "December",
                        },
                        None => panic!("wrong value for month"),
                    };
                let day: u32 = parts.get(2).unwrap().parse::<u32>().unwrap();
                let day_suffix: &str = match day {
                    1 => "st",
                    2 => "nd",
                    3 => "rd",
                    _ => "th",
                };
                format!("{} {} {}{}", year, month, day, day_suffix)
            }
            _ => self.visibility.to_string(),
        }
    }

    fn start_of_visibility_date(&self, dt: DateTime<Utc>) -> DateTime<Utc> {
        let visibility: String = self.visibility.to_string();
        if visibility == visibility::ASAP {
            return self.start_of_validition_date(dt);
        }

        match self.category {
            Category::Once => {
                let parts: Vec<&str> = visibility.split('-').collect::<Vec<_>>();
                if parts.len() == 3 {
                    let year: i32 = parts.first().unwrap().parse::<i32>().unwrap();
                    let month: u32 = parts.get(1).unwrap().parse::<u32>().unwrap();
                    let day: u32 = parts.get(2).unwrap().parse::<u32>().unwrap();
                    Utc.with_ymd_and_hms(year, month, day, 0, 0, 0).unwrap()
                } else {
                    panic!("wrong visibility string");
                }
            }

            Category::Daily => {
                // TODO: manage time of day
                panic!("wrong visibility string");
            }

            Category::Weekly => {
                let weekday: u32 = visibility
                    .parse::<Weekday>()
                    .unwrap()
                    .num_days_from_monday();

                let dt_weekday: u32 = dt.weekday().num_days_from_monday();

                let dt_with_offset = if dt_weekday > weekday {
                    dt - Duration::try_days((dt_weekday - weekday).into()).unwrap()
                } else {
                    dt + Duration::try_days((weekday - dt_weekday).into()).unwrap()
                };

                Utc.with_ymd_and_hms(dt.year(), dt.month(), dt_with_offset.day(), 0, 0, 0)
                    .unwrap()
            }

            Category::Monthly => {
                let mut day = visibility.parse::<u32>().unwrap();

                match dt.month() {
                    // 30 days months
                    4 | 6 | 9 | 11 => {
                        if day > 30 {
                            day = 30;
                        }
                    }
                    // february: 28/29 days
                    2 => {
                        if is_leap_year(dt.year()) {
                            if day > 29 {
                                day = 29;
                            }
                        } else if day > 28 {
                            day = 28;
                        }
                    }
                    // 31 days months
                    _ => {}
                }

                Utc.with_ymd_and_hms(dt.year(), dt.month(), day, 0, 0, 0)
                    .unwrap()
            }

            Category::Quarterly => {
                let parts: Vec<&str> = visibility.split('-').collect::<Vec<_>>();
                let offset_month: u32 = parts.first().unwrap().parse::<u32>().unwrap();
                let mut day: u32 = parts.get(1).unwrap().parse::<u32>().unwrap();

                let month: u32 = match dt.month() {
                    1..=3 => 1,
                    4..=6 => 4,
                    7..=9 => 7,
                    _ => 10,
                } + offset_month;

                match month {
                    // 30 days months
                    4 | 6 | 9 | 11 => {
                        if day > 30 {
                            day = 30;
                        }
                    }
                    // february: 28/29 days
                    2 => {
                        if is_leap_year(dt.year()) {
                            if day > 29 {
                                day = 29;
                            }
                        } else if day > 28 {
                            day = 28;
                        }
                    }
                    // 31 days months
                    _ => {}
                }

                Utc.with_ymd_and_hms(dt.year(), month, day, 0, 0, 0)
                    .unwrap()
            }

            Category::Yearly => {
                let parts: Vec<&str> = visibility.split('-').collect::<Vec<_>>();
                let month: u32 = parts.first().unwrap().parse::<u32>().unwrap();
                let day: u32 = parts.get(1).unwrap().parse::<u32>().unwrap();
                Utc.with_ymd_and_hms(dt.year(), month, day, 0, 0, 0)
                    .unwrap()
            }
        }
    }

    fn is_visible(&self, dt: DateTime<Utc>) -> bool {
        dt.timestamp() >= self.start_of_visibility_date(dt).timestamp()
    }

    pub fn is_visible_now(&self) -> bool {
        self.is_visible(Utc::now())
    }

    pub fn is_done(&self, dt: DateTime<Utc>) -> bool {
        self.last_commit.utc().timestamp() >= self.start_of_visibility_date(dt).timestamp()
    }

    pub fn is_done_now(&self) -> bool {
        self.is_done(Utc::now())
    }

    /// Commit is "too old" when it's not accepted for start of validation date related to previous period of validity
    pub fn commit_is_too_old(&self, dt: DateTime<Utc>) -> bool {
        if self.is_done(dt) {
            return false;
        }

        let prev_period: DateTime<Utc> =
            self.start_of_validition_date(dt) - Duration::try_seconds(1).unwrap();
        let visibility_of_prev_period: DateTime<Utc> = self.start_of_visibility_date(prev_period);
        self.last_commit.utc().timestamp() < visibility_of_prev_period.timestamp()
    }

    pub fn commit_is_too_old_now(&self) -> bool {
        self.commit_is_too_old(Utc::now())
    }

    fn start_of_validition_date(&self, dt: DateTime<Utc>) -> DateTime<Utc> {
        let validation_date: LocalResult<DateTime<Utc>> = match self.category {
            Category::Once => Utc.with_ymd_and_hms(1970, 1, 1, 0, 0, 1),

            Category::Daily => Utc.with_ymd_and_hms(dt.year(), dt.month(), dt.day(), 0, 0, 0),

            Category::Weekly => {
                let days: u32 = dt.weekday().num_days_from_monday();
                let pred_week: DateTime<Utc> =
                    dt - Duration::try_days(i64::from(days)).expect("can't subtract multiple days");
                let year: i32 = pred_week.year();
                let month: u32 = pred_week.month();
                let day: u32 = pred_week.day();
                Utc.with_ymd_and_hms(year, month, day, 0, 0, 0)
            }

            Category::Monthly => Utc.with_ymd_and_hms(dt.year(), dt.month(), 1, 0, 0, 0),

            Category::Quarterly => {
                let month: u32 = match dt.month() {
                    1..=3 => 1,
                    4..=6 => 4,
                    7..=9 => 7,
                    _ => 10,
                };
                Utc.with_ymd_and_hms(dt.year(), month, 1, 0, 0, 0)
            }

            Category::Yearly => Utc.with_ymd_and_hms(dt.year(), 1, 1, 0, 0, 0),
        };

        match validation_date {
            MappedLocalTime::Single(dt) => dt,
            MappedLocalTime::Ambiguous(dt, _) => {
                println!("daily_start_of_validity AMBIGUOUS date!!!");
                dt
            }
            MappedLocalTime::None => DateTime::<Utc>::MIN_UTC,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::category::Category;
    use super::commit::Commit;
    use super::visibility::ASAP;
    use super::*;

    #[test]
    fn time_funcs_check() {
        assert_eq!(
            DateTime::from_timestamp(0, 0).unwrap(),
            Utc.with_ymd_and_hms(1970, 1, 1, 0, 0, 0).unwrap()
        );
        assert_eq!(
            DateTime::from_timestamp(0, 0).unwrap(),
            NaiveDate::from_ymd_opt(1970, 1, 1)
                .unwrap()
                .and_hms_opt(0, 0, 0)
                .unwrap()
                .and_utc()
        );
    }

    fn mock_activity(category: Category, visibility: Visibility, commit: Commit) -> Activity {
        Activity {
            id: Id::new(None),
            title: String::from("test"),
            category,
            visibility,
            last_commit: commit,
        }
    }

    /// Once activity tests
    #[test]
    fn not_committed_once_activity() {
        let activity: Activity =
            mock_activity(Category::Once, Visibility::from(ASAP), Commit::from(0));
        assert!(!activity.is_done_now());
        assert!(activity.is_visible_now());
        assert!(activity.commit_is_too_old_now());
    }

    #[test]
    fn committed_once_activity() {
        let time: DateTime<Utc> = Utc.with_ymd_and_hms(2000, 1, 1, 1, 0, 0).unwrap();

        let activity: Activity =
            mock_activity(Category::Once, Visibility::from(ASAP), Commit::new(time));
        assert!(activity.is_done(time));
        assert!(activity.is_visible(time));
        assert!(!activity.commit_is_too_old(time));
    }

    /// Daily activity tests
    #[test]
    fn not_committed_daily_activity() {
        let activity: Activity =
            mock_activity(Category::Daily, Visibility::from(ASAP), Commit::from(0));
        assert!(!activity.is_done_now());
        assert!(activity.is_visible_now());
        assert!(activity.commit_is_too_old_now());
    }

    #[test]
    fn just_committed_daily_activity() {
        let time: DateTime<Utc> = Utc.with_ymd_and_hms(2000, 1, 1, 1, 0, 0).unwrap();

        let activity: Activity =
            mock_activity(Category::Daily, Visibility::from(ASAP), Commit::new(time));
        assert!(activity.is_done(time));
        assert!(activity.is_visible(time));
        assert!(!activity.commit_is_too_old(time));
    }

    #[test]
    fn committed_end_of_day_daily_activity() {
        let end_of_day: DateTime<Utc> = Utc.with_ymd_and_hms(2000, 1, 1, 23, 59, 59).unwrap();

        let activity: Activity = mock_activity(
            Category::Daily,
            Visibility::from(ASAP),
            Commit::new(end_of_day),
        );
        assert!(activity.is_done(end_of_day));
        assert!(activity.is_visible(end_of_day));
        assert!(!activity.commit_is_too_old(end_of_day));
    }

    #[test]
    fn committed_yesterday_daily_activity() {
        let time: DateTime<Utc> = Utc.with_ymd_and_hms(2000, 1, 2, 10, 0, 0).unwrap();
        let prev_day: DateTime<Utc> = time - Duration::try_days(1).expect("can't subtract 1 day");

        let activity: Activity = mock_activity(
            Category::Daily,
            Visibility::from(ASAP),
            Commit::new(prev_day),
        );
        assert!(!activity.is_done(time));
        assert!(activity.is_visible(time));
        assert!(!activity.commit_is_too_old(time));
    }

    #[test]
    fn committed_last_second_of_previous_day_daily_activity() {
        let time: DateTime<Utc> = Utc.with_ymd_and_hms(2000, 1, 2, 10, 0, 0).unwrap();
        let prev_day: DateTime<Utc> = time - Duration::try_days(1).unwrap();
        let end_of_prev_day: DateTime<Utc> = Utc
            .with_ymd_and_hms(
                prev_day.year(),
                prev_day.month(),
                prev_day.day(),
                23,
                59,
                59,
            )
            .unwrap();

        let activity: Activity = mock_activity(
            Category::Daily,
            Visibility::from(ASAP),
            Commit::new(end_of_prev_day),
        );
        assert!(!activity.is_done(time));
        assert!(activity.is_visible(time));
        assert!(!activity.commit_is_too_old(time));
    }

    #[test]
    fn visibility_daily_activity() {
        let time1: DateTime<Utc> = Utc.with_ymd_and_hms(2000, 1, 4, 10, 0, 0).unwrap();
        let time2: DateTime<Utc> = time1 + Duration::try_minutes(10).unwrap();
        let time0: DateTime<Utc> = time1 - Duration::try_days(1).unwrap();

        let activity: Activity =
            mock_activity(Category::Daily, Visibility::from(ASAP), Commit::from(0));
        assert!(!activity.is_done(time1));
        assert!(activity.is_visible(time1));
        assert!(activity.commit_is_too_old(time1));

        let activity2: Activity =
            mock_activity(Category::Daily, Visibility::from(ASAP), Commit::new(time1));
        assert!(activity2.is_done(time2));
        assert!(activity2.is_visible(time2));
        assert!(!activity2.commit_is_too_old(time2));

        let activity3: Activity =
            mock_activity(Category::Daily, Visibility::from(ASAP), Commit::new(time0));
        assert!(!activity3.is_done(time2));
        assert!(activity3.is_visible(time2));
        assert!(!activity3.commit_is_too_old(time2));

        // TODO: this test in unsopperted right now, but it could be an improvement
        // let activity3: Activity = mock_activity(Category::Daily, Visibility::from("10:00"), Commit::new(time_zero));
        // assert!(!activity3.is_done());
        // assert!(!activity3.is_visible(Utc.with_ymd_and_hms(2000, 1, 1, 0, 0, 0).unwrap()));
        // assert!(activity3.is_visible(Utc.with_ymd_and_hms(2000, 1, 1, 10, 0, 0).unwrap()));
        // assert!(!activity3.commit_is_too_old());
    }

    /// Weekly activity tests
    #[test]
    fn not_committed_weekly_activity() {
        let time: DateTime<Utc> = Utc.with_ymd_and_hms(2000, 1, 4, 0, 0, 0).unwrap();

        let activity: Activity =
            mock_activity(Category::Weekly, Visibility::from(ASAP), Commit::from(0));
        assert!(!activity.is_done(time));
        assert!(activity.is_visible(time));
        assert!(activity.commit_is_too_old(time));
    }

    #[test]
    fn committed_on_last_second_of_past_sunday_activity() {
        // thursday
        let time: DateTime<Utc> = Utc.with_ymd_and_hms(2024, 4, 16, 0, 0, 0).unwrap();

        let super_time: DateTime<Utc> = Utc.with_ymd_and_hms(2024, 4, 30, 0, 0, 0).unwrap();

        // sunday
        let last_second_of_prev_week: DateTime<Utc> = Utc
            .with_ymd_and_hms(
                time.year(),
                time.month(),
                time.day() - time.weekday().num_days_from_sunday(),
                23,
                59,
                59,
            )
            .unwrap();

        let activity: Activity = mock_activity(
            Category::Weekly,
            Visibility::from(ASAP),
            Commit::new(last_second_of_prev_week),
        );
        assert!(!activity.is_done(time));
        assert!(activity.is_visible(time));
        assert!(!activity.commit_is_too_old(time));
        assert!(activity.commit_is_too_old(super_time));
    }

    #[test]
    fn committed_on_first_second_of_this_monday_activity() {
        // monday
        let time: DateTime<Utc> = Utc.with_ymd_and_hms(2024, 4, 15, 0, 0, 0).unwrap();

        let activity: Activity =
            mock_activity(Category::Weekly, Visibility::from(ASAP), Commit::new(time));
        assert!(activity.is_done(time));
        assert!(activity.is_visible(time));
        assert!(!activity.commit_is_too_old(time));
    }

    #[test]
    fn visibility_weekly_activity() {
        // monday
        let time: DateTime<Utc> = Utc.with_ymd_and_hms(2024, 4, 15, 0, 0, 0).unwrap();

        // wednesday
        let time1: DateTime<Utc> = Utc.with_ymd_and_hms(2024, 4, 17, 0, 0, 0).unwrap();

        let activity: Activity = mock_activity(
            Category::Weekly,
            Visibility::from(Weekday::Wed.to_string()),
            Commit::from(0),
        );

        assert!(!activity.is_done(time));
        assert!(!activity.is_visible(time));
        assert!(activity.commit_is_too_old(time));

        assert!(!activity.is_done(time1));
        assert!(activity.is_visible(time1));
        assert!(activity.commit_is_too_old(time1));
    }

    /// Monthly activity tests
    #[test]
    fn not_committed_monthly_activity() {
        let time: DateTime<Utc> = Utc.with_ymd_and_hms(2000, 1, 1, 0, 0, 0).unwrap();

        let activity: Activity =
            mock_activity(Category::Monthly, Visibility::from(ASAP), Commit::from(0));
        assert!(!activity.is_done(time));
        assert!(activity.is_visible(time));
        assert!(activity.commit_is_too_old(time));
    }

    #[test]
    fn committed_on_last_day_of_past_month_activity() {
        let time: DateTime<Utc> = Utc.with_ymd_and_hms(2000, 2, 1, 0, 0, 0).unwrap();
        let past_month: DateTime<Utc> = Utc.with_ymd_and_hms(2000, 1, 1, 0, 0, 0).unwrap();

        let activity: Activity = mock_activity(
            Category::Monthly,
            Visibility::from(ASAP),
            Commit::new(past_month),
        );
        assert!(!activity.is_done(time));
        assert!(activity.is_visible(time));
        assert!(!activity.commit_is_too_old(time));
        assert!(activity.commit_is_too_old(Utc.with_ymd_and_hms(2000, 3, 1, 0, 0, 0).unwrap()));
    }

    #[test]
    fn visibility_monthly_activity() {
        let time: DateTime<Utc> = Utc.with_ymd_and_hms(2000, 4, 9, 23, 59, 59).unwrap();
        let time1: DateTime<Utc> = Utc.with_ymd_and_hms(2000, 4, 10, 0, 0, 0).unwrap();

        let activity: Activity =
            mock_activity(Category::Monthly, Visibility::from("10"), Commit::from(0));

        assert!(!activity.is_done(time));
        assert!(!activity.is_visible(time));
        assert!(activity.commit_is_too_old(time));

        assert!(!activity.is_done(time1));
        assert!(activity.is_visible(time1));
        assert!(activity.commit_is_too_old(time1));
    }

    #[test]
    fn visibility_last_day_monthly_activity() {
        let time: DateTime<Utc> = Utc.with_ymd_and_hms(2000, 4, 29, 23, 59, 59).unwrap();
        let time1: DateTime<Utc> = Utc.with_ymd_and_hms(2000, 4, 30, 0, 0, 0).unwrap();

        let activity: Activity =
            mock_activity(Category::Monthly, Visibility::from("31"), Commit::from(0));

        assert!(!activity.is_done(time));
        assert!(!activity.is_visible(time));
        assert!(activity.commit_is_too_old(time));

        assert!(!activity.is_done(time1));
        assert!(activity.is_visible(time1));
        assert!(activity.commit_is_too_old(time1));
    }

    /// Quarterly activity tests
    #[test]
    fn not_committed_quarterly_activity() {
        let time: DateTime<Utc> = Utc.with_ymd_and_hms(2000, 1, 1, 0, 0, 0).unwrap();

        let activity: Activity =
            mock_activity(Category::Quarterly, Visibility::from(ASAP), Commit::from(0));
        assert!(!activity.is_done(time));
        assert!(activity.is_visible(time));
        assert!(activity.commit_is_too_old(time));
    }

    #[test]
    fn committed_on_last_day_of_past_quarter_activity() {
        let time: DateTime<Utc> = Utc.with_ymd_and_hms(2000, 4, 1, 0, 0, 0).unwrap();
        let last_second_of_past_quarter: DateTime<Utc> =
            Utc.with_ymd_and_hms(2000, 3, 31, 23, 59, 59).unwrap();

        let activity: Activity = mock_activity(
            Category::Quarterly,
            Visibility::from(ASAP),
            Commit::new(last_second_of_past_quarter),
        );
        assert!(!activity.is_done(time));
        assert!(activity.is_visible(time));
        assert!(!activity.commit_is_too_old(time));
        assert!(activity.commit_is_too_old(Utc.with_ymd_and_hms(2000, 7, 1, 0, 0, 0).unwrap()));
    }

    #[test]
    fn visibility_quarterly_activity() {
        let time: DateTime<Utc> = Utc.with_ymd_and_hms(2000, 4, 1, 0, 0, 0).unwrap();
        let time1: DateTime<Utc> = Utc.with_ymd_and_hms(2000, 5, 2, 0, 0, 0).unwrap();

        let activity: Activity = mock_activity(
            Category::Quarterly,
            Visibility::from("01-01"),
            Commit::from(0),
        );
        assert!(!activity.is_done(time));
        assert!(!activity.is_visible(time));
        assert!(activity.commit_is_too_old(time));

        let activity2: Activity = mock_activity(
            Category::Quarterly,
            Visibility::from("01-01"),
            Commit::from(0),
        );
        assert!(!activity2.is_done(time1));
        assert!(activity2.is_visible(time1));
        assert!(activity2.commit_is_too_old(time1));
    }

    /// Yearly activity tests
    #[test]
    fn not_committed_yearly_activity() {
        let new_year: DateTime<Utc> = Utc.with_ymd_and_hms(2000, 1, 1, 0, 0, 0).unwrap();

        let activity: Activity =
            mock_activity(Category::Yearly, Visibility::from(ASAP), Commit::from(0));
        assert!(!activity.is_done(new_year));
        assert!(activity.is_visible(new_year));
        assert!(activity.commit_is_too_old(new_year));
    }

    #[test]
    fn committed_on_last_day_of_past_year_activity() {
        let past_year: DateTime<Utc> = Utc.with_ymd_and_hms(1999, 12, 31, 23, 59, 59).unwrap();
        let new_year: DateTime<Utc> = Utc.with_ymd_and_hms(2000, 1, 1, 0, 0, 0).unwrap();

        let activity: Activity = mock_activity(
            Category::Yearly,
            Visibility::from(ASAP),
            Commit::new(past_year),
        );
        assert!(!activity.is_done(new_year));
        assert!(activity.is_visible(new_year));
        assert!(!activity.commit_is_too_old(new_year));
        assert!(activity.commit_is_too_old(new_year + Duration::try_days(366).unwrap()));
    }

    #[test]
    fn visibility_yearly_activity() {
        let time: DateTime<Utc> = Utc.with_ymd_and_hms(2000, 6, 15, 0, 0, 0).unwrap();
        let past_year: DateTime<Utc> = Utc.with_ymd_and_hms(1999, 6, 15, 0, 0, 0).unwrap();

        let activity: Activity =
            mock_activity(Category::Yearly, Visibility::from("06-01"), Commit::from(0));
        assert!(!activity.is_done(time));
        assert!(activity.is_visible(time));
        assert!(activity.commit_is_too_old(time));

        let activity2: Activity = mock_activity(
            Category::Yearly,
            Visibility::from("06-01"),
            Commit::new(time),
        );
        assert!(activity2.is_done(time));
        assert!(activity2.is_visible(time));
        assert!(!activity2.commit_is_too_old(time));

        let activity3: Activity = mock_activity(
            Category::Yearly,
            Visibility::from("06-01"),
            Commit::new(past_year),
        );
        assert!(!activity3.is_done(time));
        assert!(activity3.is_visible(time));
        assert!(!activity3.commit_is_too_old(time));
        assert!(!activity3.is_visible(Utc.with_ymd_and_hms(2000, 2, 10, 0, 0, 0).unwrap()));
    }
}
