use rusqlite::types::{FromSql, FromSqlResult, ToSql, ToSqlOutput, ValueRef};
use rusqlite::Result;
use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, PartialOrd, Serialize, Deserialize)]
pub struct Id(Option<i64>);

impl Id {
    pub fn new(id: Option<i64>) -> Self {
        Id(id)
    }

    pub fn is_inited(&self) -> bool {
        self.0.is_some()
    }
}

impl PartialEq for Id {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}
impl Eq for Id {}

impl FromSql for Id {
    fn column_result(value: ValueRef) -> FromSqlResult<Self> {
        i64::column_result(value).map(Id::from)
    }
}

impl ToSql for Id {
    fn to_sql(&self) -> Result<ToSqlOutput> {
        match self.0 {
            Some(id) => Ok(id.into()),
            None => Ok("NULL".into()),
        }
    }
}

impl From<String> for Id {
    fn from(s: String) -> Id {
        Id::new(s.parse::<i64>().ok())
    }
}

impl From<Id> for String {
    fn from(id: Id) -> String {
        match id.0 {
            Some(i) => i.to_string(),
            None => "".to_string(),
        }
    }
}

impl From<i64> for Id {
    fn from(value: i64) -> Self {
        Id::new(Some(value))
    }
}

impl From<Id> for i64 {
    fn from(id: Id) -> i64 {
        match id.0 {
            Some(id) => id,
            None => panic!(),
        }
    }
}
