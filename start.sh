#!/usr/bin/env bash

set -e
# set -x

if [[ "$1" == "--update" ]]
then
    cargo update
fi
cargo test
cargo fmt
cargo clippy
cargo run
